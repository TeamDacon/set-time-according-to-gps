	#include <Windows.h>
	#include <stdio.h>
	#include <string.h>
	#include <time.h>
	#include <unistd.h>

	#define ONE_OVER_BILLION 1E-9
	
	// Find on which COM port the LS20031 is connected
	char* findCOM(void)
		{
	char c;
	char* COMnb;
    LONG Status;
    
    HKEY  hKey;
    DWORD dwIndex = 0;
    CHAR  Name[48];
    DWORD szName;
    UCHAR PortName[48];
    DWORD szPortName;
    DWORD Type;    

    if( RegOpenKeyEx(HKEY_LOCAL_MACHINE,
                     TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"),
                     0,
                     KEY_READ,
                     &hKey) != ERROR_SUCCESS)
    return COMnb;

    do
    { 
       szName = sizeof(Name);
       szPortName = sizeof(PortName);     
       
       Status = RegEnumValue(hKey, 
                             dwIndex++, 
                             Name, 
                             &szName, 
                             NULL, 
                             &Type,
	                         PortName, 
                             &szPortName);
       
       if((Status == ERROR_SUCCESS) )
		// Parsing the name of the Device connected on an active COM port
	   {
		int i = 0;
		while((c = Name[i++]) != '\0') 
			{
			if(Name[i] != 'P') continue;
			else
				{
				if(Name[i+1] != 'r') continue;
				else
					{
					if(Name[i+2] != 'o') continue;
					else
						{
						if((Name[i+14] >= '0')&&(Name[i+14] <= '9')) 
							{
							// Saving the COM port number
							COMnb[0] = PortName[3];
							if((PortName[4] <= '9')&&(PortName[4] >= '0')){COMnb[1] = PortName[4];COMnb[2] = '\0';}
							else COMnb[1] = '\0';
							break;
							}
						}
					}
				}
			}
	   }
    } while((Status == ERROR_SUCCESS) );
    
    RegCloseKey(hKey);
    
    return COMnb;    
		}
	
	void settimeG25(char* COMnb)
		{
			struct timespec t1, t2;
			HANDLE hComm;                          // Handle to the Serial port
			char  ComPortName[] = "\\\\.\\COM";    // Name of the Serial port(May Change) to be opened,
			BOOL  Status;                          // Status of the various operations
			DWORD dwEventMask;                     // Event mask to trigger
			char  TempChar;                        // Temperory Character
			char  SerialBuffer[256];               // Buffer Containing Rxed Data
			DWORD NoBytesRead;                     // Bytes read by ReadFile()
			int i = 0,coma = 0;
			char  hour[3], minute[3], second[7];
			char  day[3], month[3], year[5];
			char  command[100];
			year[0] = '2';
			year[1] = '0';
			
 				clock_gettime(CLOCK_REALTIME, &t1);
				strcpy( command, "plink -l root -pw acmesystems root@192.168.10.10 date" );								
				// Sending command
 				system(command);				
				
				strcpy( command, "plink -l root -pw acmesystems root@192.168.10.10 date" );								
				// Sending command
				system(command); 
				clock_gettime(CLOCK_REALTIME, &t2);
				double accum = ( t2.tv_sec - t1.tv_sec )
				+ ( t2.tv_nsec - t1.tv_nsec )
				* ONE_OVER_BILLION;
				int deci = accum/1;
				int mili = (int)((accum-deci)*1000)/1;

				
				
			printf("\n\n +==========================================+");
			printf("\n |    Serial Port  Reception (Win32 API)    |");
			printf("\n +==========================================+\n");
			// Concatenate the COM port name with the number
			ComPortName[7] = COMnb[0];
			ComPortName[8] = COMnb[1];
			if(COMnb[1] != '\0') ComPortName[9] = COMnb[2];
			/*---------------------------------- Opening the Serial Port -------------------------------------------*/

			hComm = CreateFile( ComPortName,                  // Name of the Port to be Opened
		                        GENERIC_READ | GENERIC_WRITE, // Read/Write Access
								0,                            // No Sharing, ports cant be shared
								NULL,                         // No Security
							    OPEN_EXISTING,                // Open existing port only
		                        0,                            // Non Overlapped I/O
		                        NULL);                        // Null for Comm Devices

			if (hComm == INVALID_HANDLE_VALUE)
				printf("\n    Error! - Port %s can't be opened\n", ComPortName);
			else
				printf("\n    Port %s Opened\n ", ComPortName);

			/*------------------------------- Setting the Parameters for the SerialPort ------------------------------*/

			DCB dcbSerialParams = { 0 };                         // Initializing DCB structure
			dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

			Status = GetCommState(hComm, &dcbSerialParams);      //retreives  the current settings

			if (Status == FALSE)
				printf("\n    Error! in GetCommState()");

			dcbSerialParams.BaudRate = CBR_4800;      // Setting BaudRate = 4800
			dcbSerialParams.ByteSize = 8;             // Setting ByteSize = 8
			dcbSerialParams.StopBits = ONESTOPBIT;    // Setting StopBits = 1 ONESTOPBIT
			dcbSerialParams.Parity = NOPARITY;        // Setting Parity = None

			Status = SetCommState(hComm, &dcbSerialParams);  //Configuring the port according to settings in DCB

			if (Status == FALSE)
				{
					printf("\n    Error! in Setting DCB Structure");
				}
			else //If Successfull display the contents of the DCB Structure
				{
					printf("\n\n    Setting DCB Structure Successfull\n");
					printf("\n       Baudrate = %d", dcbSerialParams.BaudRate);
					printf("\n       ByteSize = %d", dcbSerialParams.ByteSize);
					printf("\n       StopBits = %d", dcbSerialParams.StopBits);
					printf("\n       Parity   = %d", dcbSerialParams.Parity);
				}

			/*------------------------------------ Setting Timeouts --------------------------------------------------*/

			COMMTIMEOUTS timeouts = { 0 };
			timeouts.ReadIntervalTimeout         = 50;
			timeouts.ReadTotalTimeoutConstant    = 50;
			timeouts.ReadTotalTimeoutMultiplier  = 10;
			timeouts.WriteTotalTimeoutConstant   = 50;
			timeouts.WriteTotalTimeoutMultiplier = 10;

			if (SetCommTimeouts(hComm, &timeouts) == FALSE)
				printf("\n\n    Error! in Setting Time Outs");
			else
				printf("\n\n    Setting Serial Port Timeouts Successfull");

			/*------------------------------------ Setting Receive Mask ----------------------------------------------*/

			Status = SetCommMask(hComm, EV_RXCHAR); //Configure Windows to Monitor the serial device for Character Reception

			if (Status == FALSE)
				printf("\n\n    Error! in Setting CommMask");
			else
				printf("\n\n    Setting CommMask successfull");

           /*------------------------------------ Setting WaitComm() Event   ----------------------------------------*/
			printf("\n\n    Waiting for Data Reception");
			Status = WaitCommEvent(hComm, &dwEventMask, NULL); //Wait for the character to be received

			/*-------------------------- Program will Wait here till a Character is received ------------------------*/

			if (Status == FALSE)
				{
					printf("\n    Error! in Setting WaitCommEvent()");
					return;
				}
			else //If  WaitCommEvent()==True Read the RXed data using ReadFile();
				{
					i = 0;
					do
						{
							Status = ReadFile(hComm, &TempChar, sizeof(TempChar), &NoBytesRead, NULL);
							SerialBuffer[i] = TempChar;
							i++;
					    }
					while (NoBytesRead > 0);
					
					while (strstr(SerialBuffer,"$GPRMC") == NULL)
					{
					i = 0;
					do
						{
							Status = ReadFile(hComm, &TempChar, sizeof(TempChar), &NoBytesRead, NULL);
							SerialBuffer[i] = TempChar;
							i++;
					    }
					while (NoBytesRead > 0);
					}


/*  					//------------Printing the RXed String to Console----------------------

					printf("\n\n    ");
					int j =0;
					for (j = 0; j < i-1; j++)		// j < i-1 to remove the dupliated last character
						printf("%c", SerialBuffer[j]); */
				}

				//Parsing the RXed String to get the UTC time
			for(i=0;i<sizeof(SerialBuffer);i++)
				{
				if(SerialBuffer[i] != '$')continue;
				else
					{
					if(SerialBuffer[i+1] != 'G')continue;
					else
						{
						if(SerialBuffer[i+2] != 'P')continue;
						else
							{
							if(SerialBuffer[i+3] != 'R')continue;
							else
								{
							int hourint = (SerialBuffer[i+7]-'0') * 10 + SerialBuffer[i+8]-'0';
							int minuteint = (SerialBuffer[i+9]-'0') * 10 + SerialBuffer[i+10]-'0';
							int secondint = (SerialBuffer[i+11]-'0') * 10 + SerialBuffer[i+12]-'0';
							int milisecondint = (SerialBuffer[i+14]-'0') * 100 + (SerialBuffer[i+15]-'0') * 10 + SerialBuffer[i+16]-'0';
							milisecondint += mili;
							if( milisecondint >= 1000)
								{
								milisecondint-=1000;
								secondint++;
								if(secondint>=60)
									{
									secondint-=60;
									minuteint++;
									if(minuteint>=60)
										{
										minuteint-=60;
										hourint++;
										if(hourint>=24)
											{
											hourint-=24;
											}
										}
									}
								}
							secondint += deci;
							if( secondint >=60)
								{
								secondint-=60;
								minuteint++;
								if(minuteint>=60)
									{
									minuteint-=60;
									hourint++;
									if(hourint>=24)
										{
										hourint-=24;
										}
									}
								}
							hour[0] = hourint/10 + '0';
							hour[1] = hourint%10 + '0';
							minute[0] = minuteint/10 + '0';
							minute[1] = minuteint%10 + '0';;
							second[0] = secondint/10 + '0';
							second[1] = secondint%10 + '0';
							second[2] = '.';
							second[3] = milisecondint/100 + '0';
							second[4] = (milisecondint/10)%10 + '0';
							second[5] = milisecondint%10 + '0';
							
							break;
								}
							}
						}
					}
				}
			//Parsing the RXed String to get the UTC Date
			coma =0;
			for(i;i<sizeof(SerialBuffer);i++)
				{
				if((SerialBuffer[i])== ',')
					{
					coma++;
					if(coma == 9)break;
					}
				}
				day[0] = SerialBuffer[i+1];
				day[1] = SerialBuffer[i+2];
				month[0] = SerialBuffer[i+3];
				month[1] = SerialBuffer[i+4];
				year[2] = SerialBuffer[i+5];
				year[3] = SerialBuffer[i+6];
 				
				CloseHandle(hComm);//Closing the Serial Port
				strcpy( command, "plink -l root -pw acmesystems root@192.168.10.10 date +%Y%m%d -s " );								
				command[65] = '"';
				command[66] = year[0];
				command[67] = year[1];
				command[68] = year[2];
				command[69] = year[3];
				command[70] = month[0];
				command[71] = month[1];
				command[72] = day[0];
				command[73] = day[1];
				command[74] = '"';
				command[75] = '\0';
				// Sending command to set system clock on date
 				system(command);
				strcpy( command, "plink -l root -pw acmesystems root@192.168.10.10 date --set=" );								
				command[60] = '"';
				command[61] = hour[0];
				command[62] = hour[1];
				command[63] = ':';
				command[64] = minute[0];
				command[65] = minute[1];
				command[66] = ':';
				command[67] = second[0];
				command[68] = second[1];
				command[69] = second[2];
				command[70] = second[3];
				command[71] = second[4];
				command[72] = second[5];
				command[73] = '"';
				command[74] = ' ';
				command[75] = '&';
				command[76] = '&';
				command[77] = ' ';
				command[78] = 'd';
				command[79] = 'a';
				command[80] = 't';
				command[81] = 'e';
				command[82] = ' ';
				command[83] = '-';
				command[84] = '-';
				command[85] = 'r';
				command[86] = 'f';
				command[87] = 'c';
				command[88] = '-';
				command[89] = '3';
				command[90] = '3';
				command[91] = '3';
				command[92] = '9';
				command[93] = '=';
				command[94] = 'n';
				command[95] = 's';
				command[96] = '\0';
				// Sending command to set system clock in time
				system(command);
				
				// Sending command to set the hardware clock according to the system clock
				strcpy( command, "plink -l root -pw acmesystems root@192.168.10.10 hwclock --systohc" );
				system(command); 
		}

		int main (void)
		{
			settimeG25(findCOM());
			return EXIT_SUCCESS;
		}